"""
Script generates predictions, splitting original images into tiles, and assembling prediction back together
"""
import argparse
import cv2
import torch
from pathlib import Path
from tqdm import tqdm
import numpy as np
import utils
import glob
#from utils.dataset import RoboticsDataset
from utilss.dataset import RoboticsDataset
from torch.utils.data import DataLoader
from torch.nn import functional as F
from nets.M3U_net_Wmini import dinky_net

from torchvision.transforms import Compose, RandomResizedCrop, RandomRotation, RandomHorizontalFlip, ToTensor, \
    Resize, RandomAffine, ColorJitter

#IMG_SIZE = 64

def img_transform():
    return Compose([
        #Resize((IMG_SIZE, IMG_SIZE)),
        ToTensor(),
    ])

def get_model(model_path):
    
    model = dinky_net()

    state = torch.load(str(model_path))
    # state = {key.replace('module.', ''): value for key, value in state['model'].items()}
    model.load_state_dict(state)

    if torch.cuda.is_available():
        return model.cuda()

    model.eval()

    return model

def get_file_name(image_path):
    file_names = []
    name = list(image_path.glob('*'))
    for i in name:
        print(i)
        file_names.append(i)
    return file_names

def predict(model, from_file_names, batch_size, to_path,img_transform):
    loader = DataLoader(
        dataset=RoboticsDataset(from_file_names, transform=img_transform),
        shuffle=False,
        batch_size=batch_size,
        num_workers=args.workers,
        pin_memory=torch.cuda.is_available()
    )
    #print('num file_names = {}'.format(len()))
    with torch.no_grad():

        for batch_num,(inputs, paths) in enumerate(tqdm(loader, desc='Predict')):
            inputs = inputs.cuda()
            print(inputs.shape)
            outputs = model(inputs)
            print(outputs.shape)

            for i, image_name in enumerate(paths):
                
                factor = 255

                t_mask = ((outputs[i, 0]).data.cpu().numpy() * factor).astype(np.uint8)
                #t_mask = (outputs[i, 0].data.cpu().numpy() * factor).astype(np.uint8)

                h, w = t_mask.shape

                full_mask = np.zeros((64, 64))
                full_mask[0:h, 0:w] = t_mask

                instrument_folder = Path(paths[i]).parent

                (to_path / instrument_folder).mkdir(exist_ok=True, parents=True)


                cv2.imwrite(str(to_path / Path(paths[i] )), full_mask)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    arg = parser.add_argument
    arg('--model_path', type=str, default='./outputs/train_unet/0-best.pth', help='path to model folder')
    arg('--output_path', type=str, help='path to save images', default='./prediction')
    arg('--batch-size', type=int, default=128)
    arg('--fold', type=int, default=0, choices=[0, 1, 2, 3, -1], help='-1: all folds')
    arg('--workers', type=int, default=6)
    arg('--input_path',type=str, default='./patches_64_6w/images')
    args = parser.parse_args()

    model = get_model(str(Path(args.model_path)))

    file_names = get_file_name(Path(args.input_path))
    print('num file_names = {}'.format(len(file_names)))
    output_path = Path(args.output_path)
    output_path.mkdir(exist_ok=True, parents=True)

    predict(model, file_names, args.batch_size, output_path,
            img_transform=img_transform())