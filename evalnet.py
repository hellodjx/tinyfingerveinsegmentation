import torch
from torchvision.transforms import Compose, Resize, ToTensor
from torch.utils.data import DataLoader

from nets.DintyNetV4 import DintyNetV4 as MyModel
from validation import *
from dataset import MaskDataset, get_img_files
from loss import dice_loss

IMG_SIZE = 64
EXPERIMENT = 'train_unet'
OUT_DIR = 'outputs/{}'.format(EXPERIMENT)


def get_data_loaders(test_files):
    test_transform = Compose([
        # Resize((IMG_SIZE, IMG_SIZE)),
        ToTensor(),
    ])

    test_loader = DataLoader(MaskDataset(test_files, test_transform),
                             batch_size=1,
                             shuffle=False,
                             pin_memory=True,
                             num_workers=0)

    return test_loader


def get_exponent(device, model, data_loader):
    criterion = dice_loss()
    running_loss = 0.0
    jc = []
    dice = []
    sen = []
    spe = []
    ppv = []
    acc = []

    for inputs, labels in data_loader:
        inputs = inputs.to(device)
        labels = labels.to(device)

        with torch.no_grad():
            outputs = model(inputs)
            loss = criterion(outputs, labels)

            jc += get_jaccard(labels, outputs, 0.5)
            dice += get_dice(labels, outputs, 0.5)
            sen += get_sen(labels, outputs, 0.5)
            spe += get_spe(labels, outputs, 0.5)
            ppv += get_ppv(labels, outputs, 0.5)
            acc += get_acc(labels, outputs, 0.5)

            # print("inputs.size: ", inputs.size(0))
        running_loss += loss.item() * inputs.size(0)

        # jc = jc / len(data_loader.dataset)
        # dice = dice / len(data_loader.dataset)
    test_js = np.mean(jc).astype(np.float64)
    test_dice = np.mean(dice).astype(np.float64)
    test_sen = np.mean(sen).astype(np.float64)
    test_spe = np.mean(spe).astype(np.float64)
    test_ppv = np.mean(ppv).astype(np.float64)
    test_acc = np.mean(acc).astype(np.float64)

    epoch_loss = running_loss / len(data_loader.dataset)

    return epoch_loss, test_js, test_dice, test_sen, test_spe, test_ppv, test_acc


def eval_date():
    image_files = get_img_files()
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    data_loader = get_data_loaders(image_files)

    model = MyModel()
    model.load_state_dict(torch.load('{}/{}-best.pth'.format(OUT_DIR, 0), map_location='cuda:0'))
    # state = torch.load('{}/last.pth.tar'.format(OUT_DIR),map_location='cuda:0')
    # state = {key.replace('module.', ''): value for key, value in state['state_dict'].items()}
    # model.load_state_dict(state)
    model.to(device)
    model.eval()

    test_epoch_loss, test_jc, test_dice, test_sen, test_spe, test_ppv, test_acc = get_exponent(device, model,
                                                                                               data_loader)
    print(
        "test_date:     test_epoch_loss: {}      test_jc:{}       test_dice:{}    test_sen:{}      test_spe:{}      test_ppv:{}      test_acc:{}       ".format(
            test_epoch_loss, test_jc, test_dice, test_sen, test_spe, test_ppv, test_acc))


if __name__ == "__main__":
    eval_date()
