import argparse
import logging
import os
import configparser

import numpy as np
import pandas as pd
import torch
from sklearn.model_selection import KFold
from tensorboardX import SummaryWriter
from torch.optim import Adam
from torch.utils.data import DataLoader
from torchsummaryX import summary
from torchvision.transforms import Compose, ToTensor

from dataset import MaskDataset, get_img_files
from loss import dice_loss
from nets.DintyNetV4 import DintyNetV4 as MyModel
from trainer import Trainer

np.random.seed(1)
torch.backends.cudnn.deterministic = True
torch.manual_seed(1)
# opencev读不了gif图片
N_CV = 5
BATCH_SIZE = 256
LR = 0.01

N_EPOCHS = 500
IMG_SIZE = 64
RANDOM_STATE = 1

config = configparser.RawConfigParser()
config.read('configuration.txt')
name_experiment = config.get('experiment name', 'name')
OUT_DIR = 'outputs/{}'.format(name_experiment)


def get_data_loaders(train_files, val_files, img_size=IMG_SIZE):
    train_transform = Compose([
        # ColorJitter(0.3, 0.3, 0.3, 0.3),
        # RandomResizedCrop(img_size, scale=(0.8, 1.2)),
        # RandomAffine(10.),
        # RandomRotation(13.),
        # RandomHorizontalFlip(),
        ToTensor(),
    ])
    # train_mask_transform = Compose([
    #     RandomResizedCrop(img_size, scale=(0.8, 1.2)),
    #     RandomAffine(10.),
    #     RandomRotation(13.),
    #     RandomHorizontalFlip(),
    #     ToTensor(),
    # ])
    val_transform = Compose([
        # Resize((64, 64)),
        ToTensor(),
    ])

    train_loader = DataLoader(MaskDataset(train_files, train_transform),
                              batch_size=BATCH_SIZE,
                              shuffle=True,
                              pin_memory=True,
                              num_workers=16)
    val_loader = DataLoader(MaskDataset(val_files, val_transform),
                            batch_size=BATCH_SIZE,
                            shuffle=False,
                            pin_memory=True,
                            num_workers=16)

    # test_loader = DataLoader(TEST_MaskDataset(test_files, val_transform),
    #                         batch_size=BATCH_SIZE,
    #                         shuffle=False,
    #                         pin_memory=True,
    #                         num_workers=16)

    return train_loader, val_loader


def save_best_model(cv, model, df_hist):
    if df_hist['val_loss'].tail(1).iloc[0] <= df_hist['val_loss'].min():
        torch.save(model.state_dict(), '{}/{}-best.pth'.format(OUT_DIR, cv))


def write_on_board(writer, df_hist):
    row = df_hist.tail(1).iloc[0]

    writer.add_scalars('{}/loss'.format(name_experiment), {
        'train': row.train_loss,
        'val': row.val_loss,
    }, row.epoch)


def log_hist(df_hist):
    last = df_hist.tail(1)
    best = df_hist.sort_values('val_loss').head(1)
    summary = pd.concat((last, best)).reset_index(drop=True)
    summary['name'] = ['Last', 'Best']
    logger.debug(summary[['name', 'epoch', 'train_loss', 'dice', 'val_loss', 'val_dice']])
    logger.debug('')


def run_cv(img_size):
    image_files = get_img_files()  # dataset

    kf = KFold(n_splits=N_CV, random_state=RANDOM_STATE, shuffle=True)
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    for n, (train_idx, val_idx) in enumerate(kf.split(image_files)):
        train_files = image_files[train_idx]
        val_files = image_files[val_idx]
        writer = SummaryWriter()

        def on_after_epoch(m, df_hist):
            save_best_model(n, m, df_hist)
            write_on_board(writer, df_hist)
            log_hist(df_hist)

        # mod scale = 2 to None
        criterion = dice_loss()

        data_loaders = get_data_loaders(train_files, val_files, img_size)
        trainer = Trainer(data_loaders, criterion, device, on_after_epoch)

        model = MyModel()
        print(model)
        summary(model, torch.zeros((256, 1, 64, 64)))  # torch.zeros(batch_size,第一层输入通道，size_h,size_w)
        model.to(device)
        optimizer = Adam(model.parameters(), lr=LR)

        hist = trainer.train(model, optimizer, num_epochs=N_EPOCHS)
        hist.to_csv('{}/{}-hist.csv'.format(OUT_DIR, n), index=False)

        writer.close()


if __name__ == '__main__':
    if not os.path.exists(OUT_DIR):
        os.makedirs(OUT_DIR)

    logger = logging.getLogger("logger")
    logger.setLevel(logging.DEBUG)
    if not logger.hasHandlers():
        logger.addHandler(logging.FileHandler(filename="outputs/{}.log".format(name_experiment)))

    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--img_size',
        type=int,
        default=IMG_SIZE,
        help='image size',
    )
    # parser.add_argument(
    #   '--pre_trained',
    #  type=str,
    # help='path of pre trained weight',
    # )
    args, _ = parser.parse_known_args()
    print(args)
    run_cv(**vars(args))
