import pandas as pd
from validation import *
from bar import Bar


class Trainer:
    def __init__(self, data_loaders, criterion, device, on_after_epoch=None):
        self.data_loaders = data_loaders
        self.criterion = criterion
        self.device = device
        self.history = []
        self.on_after_epoch = on_after_epoch

    def train(self, model, optimizer, num_epochs):
        for epoch in range(num_epochs):
            print(f"Train epoch: {epoch + 1}")
            train_epoch_loss, jc, dice, sen, spe, ppv, acc = self._train_on_epoch(model, optimizer)
            print(f"Val epoch: {epoch + 1}")
            val_epoch_loss, val_jc, val_dice, val_sen, val_spe, val_ppv, val_acc = self._val_on_epoch(model, optimizer)

            hist = {
                'epoch': epoch,
                'train_loss': train_epoch_loss,
                # 'jaccard': jc,
                'dice': dice,
                # 'sen': sen,
                # 'spe': spe,
                # 'ppv': ppv,
                'acc': acc,
                'val_loss': val_epoch_loss,
                'val_jaccard': val_jc,
                'val_dice': val_dice,
                # 'val_sen': sen,
                # 'val_spe': spe,
                # 'val_ppv': ppv,
                # 'val_acc': acc

            }
            self.history.append(hist)

            if self.on_after_epoch is not None:
                self.on_after_epoch(model, pd.DataFrame(self.history))
                print(
                    "train_epoch_loss:{}      jc:{}       dice:{}     sen:{}      spe:{}      ppv:{}     "
                    " acc:{}".format(
                        train_epoch_loss, jc, dice, sen, spe, ppv, acc))
                print(
                    "val_epoch_loss: {}      val_jc:{}       val_dice:{}    val_sen:{}      val_spe:{}   "
                    "   val_ppv:{}      val_acc:{}".format(
                        val_epoch_loss, val_jc, val_dice, val_sen, val_spe, val_ppv, val_acc))
        # print(self.history)
        return pd.DataFrame(self.history)

    def _train_on_epoch(self, model, optimizer):
        model.train()
        data_loader = self.data_loaders[0]
        running_loss = 0.0
        jc = []
        dice = []
        sen = []
        spe = []
        ppv = []
        acc = []

        for inputs, labels in Bar(data_loader):
            inputs = inputs.to(self.device)
            labels = labels.to(self.device)

            optimizer.zero_grad()

            with torch.set_grad_enabled(True):
                outputs = model(inputs)
                loss = self.criterion(outputs, labels)
                loss.backward()
                optimizer.step()

                jc += get_jaccard(labels, outputs, 0.5)
                dice += get_dice(labels, outputs, 0.5)
                sen += get_sen(labels, outputs, 0.5)
                spe += get_spe(labels, outputs, 0.5)
                ppv += get_ppv(labels, outputs, 0.5)
                acc += get_acc(labels, outputs, 0.5)

            # print("inputs.size: ", inputs.size(0))
            running_loss += loss.item() * inputs.size(0)

        # jc = jc / len(data_loader.dataset)
        # dice = dice / len(data_loader.dataset)
        mean_js = np.mean(jc).astype(np.float64)
        mean_dice = np.mean(dice).astype(np.float64)
        mean_sen = np.mean(sen).astype(np.float64)
        mean_spe = np.mean(spe).astype(np.float64)
        mean_ppv = np.mean(ppv).astype(np.float64)
        mean_acc = np.mean(acc).astype(np.float64)

        epoch_loss = running_loss / len(data_loader.dataset)

        return epoch_loss, mean_js, mean_dice, mean_sen, mean_spe, mean_ppv, mean_acc

    def _val_on_epoch(self, model, optimizer):
        model.eval()
        data_loader = self.data_loaders[1]
        running_loss = 0.0
        jc = []
        dice = []
        sen = []
        spe = []
        ppv = []
        acc = []

        for inputs, labels in Bar(data_loader):
            inputs = inputs.to(self.device)
            labels = labels.to(self.device)

            optimizer.zero_grad()

            with torch.set_grad_enabled(False):
                outputs = model(inputs)
                loss = self.criterion(outputs, labels)

                jc += get_jaccard(labels, outputs, 0.5)
                dice += get_dice(labels, outputs, 0.5)
                sen += get_sen(labels, outputs, 0.5)
                spe += get_spe(labels, outputs, 0.5)
                ppv += get_ppv(labels, outputs, 0.5)
                acc += get_acc(labels, outputs, 0.5)

                # print("inputs.size: ", inputs.size(0))
            running_loss += loss.item() * inputs.size(0)

            # jc = jc / len(data_loader.dataset)
            # dice = dice / len(data_loader.dataset)
            val_js = np.mean(jc).astype(np.float64)
            val_dice = np.mean(dice).astype(np.float64)
            val_sen = np.mean(sen).astype(np.float64)
            val_spe = np.mean(spe).astype(np.float64)
            val_ppv = np.mean(ppv).astype(np.float64)
            val_acc = np.mean(acc).astype(np.float64)

        epoch_loss = running_loss / len(data_loader.dataset)

        return epoch_loss, val_js, val_dice, val_sen, val_spe, val_ppv, val_acc
