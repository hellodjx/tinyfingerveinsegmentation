import numpy as np
import torch
import utils

from torch import nn


# result_dir = './valid_dir/'

def get_jaccard(y_true, y_pred, threshold=0.5):
    # y_pred = F.sigmoid(y_pred)
    y_pred = (y_pred > threshold).float()
    y_true = (y_true > threshold).float()
    epsilon = 1e-6
    intersection = (y_pred * y_true).sum(dim=-2).sum(dim=-1)
    union = (y_pred + y_true).sum(dim=-2).sum(dim=-1)
    return list((intersection / (union - intersection + epsilon)).data.cpu().numpy())


def get_dice(y_true, y_pred, threshold=0.5):
    # y_pred = F.sigmoid(y_pred)
    y_pred = (y_pred > threshold).float()
    y_true = (y_true > threshold).float()
    epsilon = 1e-6
    intersection = (y_pred * y_true).sum(dim=-2).sum(dim=-1)
    union = (y_pred + y_true).sum(dim=-2).sum(dim=-1)
    return list(((2. * intersection) / (union + epsilon)).data.cpu().numpy())


def get_sen(y_true, y_pred, threshold=0.5):
    # y_pred = F.sigmoid(y_pred)
    y_pred = (y_pred > threshold).float()
    y_true = (y_true > threshold).float()
    epsilon = 1e-6
    TP = (((y_pred == 1).float() + (y_true == 1).float()) == 2.).float().sum(dim=-2).sum(dim=-1)
    FN = (((y_pred == 0).float() + (y_true == 1).float()) == 2.).float().sum(dim=-2).sum(dim=-1)
    return list((TP / (TP + FN + epsilon)).data.cpu().numpy())


def get_spe(y_true, y_pred, threshold=0.5):
    # y_pred = F.sigmoid(y_pred)
    y_pred = (y_pred > threshold).float()
    y_true = (y_true > threshold).float()
    epsilon = 1e-6
    TN = (((y_pred == 0).float() + (y_true == 0).float()) == 2.).float().sum(dim=-2).sum(dim=-1)
    FP = (((y_pred == 1).float() + (y_true == 0).float()) == 2.).float().sum(dim=-2).sum(dim=-1)
    return list((TN / (TN + FP + epsilon)).data.cpu().numpy())


def get_ppv(y_true, y_pred, threshold=0.5):
    # y_pred = F.sigmoid(y_pred)
    y_pred = (y_pred > threshold).float()
    y_true = (y_true > threshold).float()
    epsilon = 1e-6
    TP = (((y_pred == 1).float() + (y_true == 1).float()) == 2.).float().sum(dim=-2).sum(dim=-1)
    FP = (((y_pred == 1).float() + (y_true == 0).float()) == 2.).float().sum(dim=-2).sum(dim=-1)
    return list((TP / (TP + FP + epsilon)).data.cpu().numpy())


def get_acc(y_true, y_pred, threshold=0.5):
    y_pred = (y_pred > threshold).float()
    y_true = (y_true > threshold).float()
    epsilon = 1e-6
    TP = (((y_pred == 1).float() + (y_true == 1).float()) == 2.).float().sum(dim=-2).sum(dim=-1)
    FN = (((y_pred == 0).float() + (y_true == 1).float()) == 2.).float().sum(dim=-2).sum(dim=-1)
    TN = (((y_pred == 0).float() + (y_true == 0).float()) == 2.).float().sum(dim=-2).sum(dim=-1)
    FP = (((y_pred == 1).float() + (y_true == 0).float()) == 2.).float().sum(dim=-2).sum(dim=-1)
    return list(((TP + TN) / (TP + FP + FN + TN)).data.cpu().numpy())


def validation_binary(model, criterion, valid_dataset, num_classes=None):
    with torch.no_grad():
        model.eval()
        losses = []
        jaccard = []
        dice = []
        sen = []
        spe = []
        ppv = []

        for i, (inputs, targets) in enumerate(valid_dataset):
            inputs = utils.cuda(inputs)
            targets = utils.cuda(targets)
            outputs = model(inputs)
            loss = criterion(outputs, targets)
            losses.append(loss.item())
            jaccard += get_jaccard(targets, outputs.float(), 0.3)
            dice += get_dice(targets, outputs.float(), 0.3)
            sen += get_sen(targets, outputs.float(), 0.3)
            spe += get_spe(targets, outputs.float(), 0.3)
            ppv += get_ppv(targets, outputs.float(), 0.3)

            # torchvision.utils.save_image(inputs.data.cpu(),
        # 								os.path.join(result_dir,
        # 											'valid_%d_image.png'%(i)))
        # torchvision.utils.save_image(outputs.data.cpu(),
        # 								os.path.join(result_dir,
        # 											'valid_%d_SR.png'%(i)))
        # torchvision.utils.save_image(targets.data.cpu(),
        # 								os.path.join(result_dir,
        # 											'valid_%d_GT.png'%(i)))

        valid_loss = np.mean(losses)
        valid_jaccard = np.mean(jaccard).astype(np.float64)
        valid_dice = np.mean(dice).astype(np.float64)
        valid_se = np.mean(sen).astype(np.float64)
        valid_sp = np.mean(spe).astype(np.float64)
        valid_pp = np.mean(ppv).astype(np.float64)

        print('Valid loss: {:.5f} | Jaccard: {:.5f} | DSC: {:.5f} | SEN: {:.5f} | SPE： {:.5f} | PPV: {:.5f}'.
              format(valid_loss, valid_jaccard, valid_dice, valid_se, valid_sp, valid_pp))

        metrics = {'valid_loss': valid_loss,
                   'jaccard_loss': valid_jaccard,
                   'dice': valid_dice,
                   'sen': valid_se,
                   'spe': valid_sp,
                   'ppv': valid_pp
                   }

        return metrics


def validation_multi(model: nn.Module, criterion, valid_loader, num_classes):
    with torch.no_grad():
        model.eval()
        losses = []
        confusion_matrix = np.zeros(
            (num_classes, num_classes), dtype=np.uint32)
        for inputs, targets in valid_loader:
            inputs = utils.cuda(inputs)
            targets = utils.cuda(targets)
            outputs = model(inputs)
            loss = criterion(outputs, targets)
            losses.append(loss.item())
            output_classes = outputs.data.cpu().numpy().argmax(axis=1)
            target_classes = targets.data.cpu().numpy()
            confusion_matrix += calculate_confusion_matrix_from_arrays(
                output_classes, target_classes, num_classes)

        confusion_matrix = confusion_matrix[1:, 1:]  # exclude background
        valid_loss = np.mean(losses)
        ious = {'iou_{}'.format(cls + 1): iou
                for cls, iou in enumerate(calculate_iou(confusion_matrix))}

        dices = {'dice_{}'.format(cls + 1): dice
                 for cls, dice in enumerate(calculate_dice(confusion_matrix))}

        average_iou = np.mean(list(ious.values()))
        average_dices = np.mean(list(dices.values()))

        print(
            'Valid loss: {:.4f}, average IoU: {:.4f}, average Dice: {:.4f}'.format(valid_loss,
                                                                                   average_iou,
                                                                                   average_dices))
        metrics = {'valid_loss': valid_loss, 'iou': average_iou}
        metrics.update(ious)
        metrics.update(dices)
        return metrics


def calculate_confusion_matrix_from_arrays(prediction, ground_truth, nr_labels):
    replace_indices = np.vstack((
        ground_truth.flatten(),
        prediction.flatten())
    ).T
    confusion_matrix, _ = np.histogramdd(
        replace_indices,
        bins=(nr_labels, nr_labels),
        range=[(0, nr_labels), (0, nr_labels)]
    )
    confusion_matrix = confusion_matrix.astype(np.uint32)
    return confusion_matrix


def calculate_iou(confusion_matrix):
    ious = []
    for index in range(confusion_matrix.shape[0]):
        true_positives = confusion_matrix[index, index]
        false_positives = confusion_matrix[:, index].sum() - true_positives
        false_negatives = confusion_matrix[index, :].sum() - true_positives
        denom = true_positives + false_positives + false_negatives
        if denom == 0:
            iou = 0
        else:
            iou = float(true_positives) / denom
        ious.append(iou)
    return ious


def calculate_dice(confusion_matrix):
    dices = []
    for index in range(confusion_matrix.shape[0]):
        true_positives = confusion_matrix[index, index]
        false_positives = confusion_matrix[:, index].sum() - true_positives
        false_negatives = confusion_matrix[index, :].sum() - true_positives
        denom = 2 * true_positives + false_positives + false_negatives
        if denom == 0:
            dice = 0
        else:
            dice = 2 * float(true_positives) / denom
        dices.append(dice)
    return dices
