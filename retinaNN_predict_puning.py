###################################################
#
#   Script to
#   - Calculate prediction of the test dataset
#   - Calculate the parameters to evaluate the prediction
#
##################################################
# Python
import configparser
import time
import os

from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix
from sklearn.metrics import f1_score
from sklearn.metrics import jaccard_similarity_score
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve
from sklearn.utils.multiclass import type_of_target

from dataset import get_test_img_files
from evalnet import get_data_loaders
from utils.extract_patches import get_data_testing
from utils.extract_patches import get_data_testing_overlap
# from utils.extract_patches import kill_border
from utils.extract_patches import pred_only_FOV
from utils.extract_patches import recompone
from utils.extract_patches import recompone_overlap
from utils.help_functions import *
from utils.pre_processing import my_PreProc
from nets.DintyNetV4 import DintyNetV4 as MyModel
from validation import *

torch.cuda.set_device(0)
# 加载配置文件，解析参数
# ========= CONFIG FILE TO READ FROM =======
config = configparser.RawConfigParser()
config.read('configuration.txt')
# ===========================================
# run the training on invariant or local
path_data = config.get('data paths', 'path_local')  # 数据路径
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
image_files = get_test_img_files()  # 获取文件名
data_loader = get_data_loaders(image_files)
test_imgs_orig_two = []
DRIVE_test_border_masks = []

for inputs, labels in data_loader:
    inputs = np.squeeze(inputs)
    labels = np.squeeze(labels)
    inputs = np.expand_dims(inputs, axis=0)
    labels = np.expand_dims(labels, axis=0)

    # print(inputs.shape)
    test_imgs_orig_two.append(inputs)

    DRIVE_test_border_masks.append(labels)
test_imgs_orig_two = np.array(test_imgs_orig_two)
DRIVE_test_border_masks = np.array(DRIVE_test_border_masks)
print(DRIVE_test_border_masks)
print(test_imgs_orig_two.shape)
# original test images (for FOV selection)
test_imgs_orig = test_imgs_orig_two
full_img_height = test_imgs_orig.shape[2]
full_img_width = test_imgs_orig.shape[3]
# the border masks provided by the DRIVE

# dimension of the patches
patch_height = int(config.get('data attributes', 'patch_height'))
patch_width = int(config.get('data attributes', 'patch_width'))
# the stride in case output with average 图像分块的跳跃步长
stride_height = int(config.get('testing settings', 'stride_height'))
stride_width = int(config.get('testing settings', 'stride_width'))
assert (stride_height < patch_height and stride_width < patch_width)  # 确定是否跳跃步长宽和高小于分块宽和高
# model name
name_experiment = config.get('experiment name', 'name')
OUT_DIR = 'outputs/{}'.format(name_experiment)
path_experiment = os.path.join("test_puning_result", name_experiment)
# N full images to be predicted
Imgs_to_test = int(config.get('testing settings', 'full_images_to_test'))  # 20张图像全部进行预测
# Grouping of the predicted images(How many original-groundTruth-prediction images are visualized in each image)
N_visual = int(config.get('testing settings', 'N_group_visual'))  # 1
# ====== average mode ===========（Compute average in the prediction, improve results but require more patches to be predicted）
average_mode = config.getboolean('testing settings', 'average_mode')  # average=True

# #ground truth
# gtruth= path_data + config.get('data paths', 'test_groundTruth')#测试集标签封装文件
# img_truth= load_hdf5(gtruth)#测试集标签图像
# visualize(group_images(test_imgs_orig[0:20,:,:,:],5),'original')#.show()#显示所有的测试图像
# visualize(group_images(test_border_masks[0:20,:,:,:],5),'borders')#.show()#显示所有的掩膜图像
# visualize(group_images(img_truth[0:20,:,:,:],5),'gtruth')#.show()#显示所有的标签图像


# ============ Load the data and divide in patches
# 图像分块,预测
patches_imgs_test = None
new_height = None
new_width = None
masks_test = None
patches_masks_test = None
labels = None
if average_mode == True:
    patches_imgs_test, labels, new_height, new_width, masks_test = get_data_testing_overlap(
        DRIVE_test_imgs_original=test_imgs_orig_two,  # original
        DRIVE_test_groudTruth=DRIVE_test_border_masks,  # masks
        Imgs_to_test=int(config.get('testing settings', 'full_images_to_test')),
        patch_height=patch_height,
        patch_width=patch_width,
        stride_height=stride_height,
        stride_width=stride_width
    )
else:
    patches_imgs_test, patches_masks_test = get_data_testing(
        DRIVE_test_imgs_original=DRIVE_test_imgs_original,  # original
        DRIVE_test_groudTruth=path_data + config.get('data paths', 'test_groundTruth'),  # masks
        Imgs_to_test=int(config.get('testing settings', 'full_images_to_test')),
        patch_height=patch_height,
        patch_width=patch_width,
    )
# 前者是采用覆盖式的图像块获取方法，后者就是简单的拼凑式

print(patches_imgs_test.shape)

print(np.max(patches_imgs_test))
print(np.min(patches_imgs_test))

# ================ Run the prediction of the patches ==================================
best_last = config.get('testing settings',
                       'best_last')  # Choose the model to test: best==epoch with min loss, last==last epoch best_last=best
# Load the saved model 加载已经训练好的模型 和 相关的权重

model = MyModel()
model.load_state_dict(torch.load('outputs\\checkpoint.M3u.2020-09-11-1491.pth.tar')["state_dict"])
model.to(device)
model.eval()
# model = model_from_json(open(path_experiment+name_experiment +'_architecture.json').read())
# model.load_weights(path_experiment+name_experiment + '_'+best_last+'_weights.h5')
# Calculate the predictions 进行模型预测

num = patches_imgs_test.shape[0]
pred_patches = np.zeros((num, 1, 64, 64))
batch_sizes = 64
frequency = int(num / batch_sizes)
yu = num % batch_sizes
print(frequency)
dice = []
runtime = 0
for i in range(frequency + 1):
    with torch.no_grad():
        if (i == frequency):
            inputs = (torch.from_numpy(patches_imgs_test[i * batch_sizes:, :, :, :]).type(torch.FloatTensor)).cuda()
            start１ = time.time()
            outputs = (model(inputs))
            end１ = time.time()
            runtime += end１ - start１
            outputs = outputs.cpu().numpy()
            dice += get_dice(torch.from_numpy(labels[i * batch_sizes:, :, :, :]), torch.from_numpy(outputs), 0.5)
            pred_patches[i * batch_sizes:, :, :, :] = outputs
            break

        inputs = (torch.from_numpy(patches_imgs_test[i * batch_sizes:(i + 1) * batch_sizes, :, :, :]).type(
            torch.FloatTensor)).cuda()
        start = time.time()
        outputs = model(inputs)
        end = time.time()
        runtime += end - start
        outputs = outputs.cpu().numpy()
        dice += get_dice(torch.from_numpy(labels[i * batch_sizes:(i + 1) * batch_sizes, :, :, :]),
                         torch.from_numpy(outputs), 0.5)

        pred_patches[i * batch_sizes:(i + 1) * batch_sizes, :, :, :] = outputs
test_dice = np.mean(dice).astype(np.float64)
print('-------------------------------------------')
print("总时间:" + str(runtime))
print('dice:' + str(test_dice))
# predictions[:,:,:]=predictions[:,:,1]
# pred_patches = np.reshape(predictions,(20240,1,48,48))
# pred_patches = np.reshape(np.array(pred_patches),(num,1,64,64))
# ===== Convert the prediction arrays in corresponding images 转换相应图像中的预测数组
# pred_patches = pred_to_imgs(predictions, patch_height, patch_width, "original")

print(pred_patches.shape)
print(np.max(pred_patches))
print(np.min(pred_patches))

# ========== Elaborate and visualize the predicted images ====================
pred_imgs = None
orig_imgs = None
gtruth_masks = None
if average_mode == True:
    pred_imgs = recompone_overlap(pred_patches, new_height, new_width, stride_height, stride_width)  # predictions
    orig_imgs = my_PreProc(test_imgs_orig[0:pred_imgs.shape[0], :, :, :])  # originals
    gtruth_masks = masks_test  # ground truth masks
else:
    pred_imgs = recompone(pred_patches, 13, 12)  # predictions
    orig_imgs = recompone(patches_imgs_test, 13, 12)  # originals
    gtruth_masks = recompone(patches_masks_test, 13, 12)  # masks

print(pred_imgs.shape)
print(np.max(pred_imgs))
print(np.min(pred_imgs))

# apply the DRIVE masks on the repdictions #set everything outside the FOV to zero!!
# 对于预测的数据将掩膜外的数据清零
# kill_border(pred_imgs, test_border_masks)  #DRIVE MASK  #only for visualization
## back to original dimensions
orig_imgs = orig_imgs[:, :, 0:full_img_height, 0:full_img_width]
pred_imgs = pred_imgs[:, :, 0:full_img_height, 0:full_img_width]
gtruth_masks = gtruth_masks[:, :, 0:full_img_height, 0:full_img_width]
print("Orig imgs shape: " + str(orig_imgs.shape))
print("pred imgs shape: " + str(pred_imgs.shape))
print("Gtruth imgs shape: " + str(gtruth_masks.shape))
visualize(group_images(orig_imgs, N_visual), os.path.join(path_experiment, "all_originals"))  # .show()
visualize(group_images(pred_imgs, N_visual), os.path.join(path_experiment, "all_predictions"))  # .show()
visualize(group_images(gtruth_masks, N_visual), os.path.join(path_experiment, "all_groundTruths"))  # .show()
# visualize results comparing mask and prediction:
assert (orig_imgs.shape[0] == pred_imgs.shape[0] and orig_imgs.shape[0] == gtruth_masks.shape[0])
N_predicted = orig_imgs.shape[0]
group = N_visual
assert (N_predicted % group == 0)
for i in range(int(N_predicted / group)):
    orig_stripe = group_images(orig_imgs[i * group:(i * group) + group, :, :, :], group)
    # masks_stripe = group_images(gtruth_masks[i*group:(i*group)+group,:,:,:],group)
    pred_stripe = group_images(pred_imgs[i * group:(i * group) + group, :, :, :], group)
    total_img = np.concatenate((orig_stripe, pred_stripe), axis=0)
    visualize(total_img, os.path.join(path_experiment, "_Original_GroundTruth_Prediction" + str(i)))  # .show()

# ====== Evaluate the results
print("\n\n========  Evaluate the results =======================")
# predictions only inside the FOV 只预测FOV内部的图像
y_scores, y_true = pred_only_FOV(pred_imgs, gtruth_masks)  # returns data only inside the FOV
print(y_scores.shape)
print(max(y_scores))
print(min(y_scores))
print(y_true.shape)
print(max(y_true))
print(min(y_true))
print(type_of_target(y_true))
print(y_true.dtype)
print("Calculating results only inside the FOV:")
print("y scores pixels: " + str(
    y_scores.shape[0]) + " (radius 270: 270*270*3.14==228906), including background around retina: " + str(
    pred_imgs.shape[0] * pred_imgs.shape[2] * pred_imgs.shape[3]) + " (584*565==329960)")
print("y true pixels: " + str(
    y_true.shape[0]) + " (radius 270: 270*270*3.14==228906), including background around retina: " + str(
    gtruth_masks.shape[2] * gtruth_masks.shape[3] * gtruth_masks.shape[0]) + " (584*565==329960)")

# Area under the ROC curve
fpr, tpr, thresholds = roc_curve((y_true), y_scores)
AUC_ROC = roc_auc_score(y_true, y_scores)
# test_integral = np.trapz(tpr,fpr) #trapz is numpy integration 
print("\nArea under the ROC curve: " + str(AUC_ROC))
roc_curve = plt.figure()
plt.plot(fpr, tpr, '-', label='Area Under the Curve (AUC = %0.4f)' % AUC_ROC)
plt.title('ROC curve')
plt.xlabel("FPR (False Positive Rate)")
plt.ylabel("TPR (True Positive Rate)")
plt.legend(loc="lower right")
plt.savefig(os.path.join(path_experiment, "ROC.png"))

# Precision-recall curve
precision, recall, thresholds = precision_recall_curve(y_true, y_scores)
precision = np.fliplr([precision])[0]  # so the array is increasing (you won't get negative AUC)
recall = np.fliplr([recall])[0]  # so the array is increasing (you won't get negative AUC)
AUC_prec_rec = np.trapz(precision, recall)  # 求数值积分
print("\nArea under Precision-Recall curve: " + str(AUC_prec_rec))
prec_rec_curve = plt.figure()
plt.plot(recall, precision, '-', label='Area Under the Curve (AUC = %0.4f)' % AUC_prec_rec)
plt.title('Precision - Recall curve')
plt.xlabel("Recall")
plt.ylabel("Precision")
plt.legend(loc="lower right")
plt.savefig(os.path.join(path_experiment, "Precision_recall.png"))

# Confusion matrix
threshold_confusion = 0.5
print("\nConfusion matrix:  Custom threshold (for positive) of " + str(threshold_confusion))
y_pred = np.empty((y_scores.shape[0]))
for i in range(y_scores.shape[0]):
    if y_scores[i] >= threshold_confusion:
        y_pred[i] = 1
    else:
        y_pred[i] = 0
print("yue" + str(y_true.shape))
# dice = get_dice(y_true, y_pred)
# print("diec:"+str(dice))
confusion = confusion_matrix(y_true, y_pred)
print(confusion)
accuracy = 0
if float(np.sum(confusion)) != 0:  # 令分母有意义
    accuracy = float(confusion[0, 0] + confusion[1, 1]) / float(np.sum(confusion))
print("Global Accuracy: " + str(accuracy))
specificity = 0
if float(confusion[0, 0] + confusion[0, 1]) != 0:
    specificity = float(confusion[0, 0]) / float(confusion[0, 0] + confusion[0, 1])
print("Specificity: " + str(specificity))
sensitivity = 0
if float(confusion[1, 1] + confusion[1, 0]) != 0:
    sensitivity = float(confusion[1, 1]) / float(confusion[1, 1] + confusion[1, 0])
print("Sensitivity: " + str(sensitivity))
precision = 0
if float(confusion[1, 1] + confusion[0, 1]) != 0:
    precision = float(confusion[1, 1]) / float(confusion[1, 1] + confusion[0, 1])
print("Precision: " + str(precision))

# Jaccard similarity index
jaccard_index = jaccard_similarity_score(y_true, y_pred, normalize=True)
print("\nJaccard similarity score: " + str(jaccard_index))

# F1 score
F1_score = f1_score(y_true, y_pred, labels=None, average='binary', sample_weight=None)
print("\nF1 score (F-measure): " + str(F1_score))

# Save the results
with open(os.path.join(path_experiment, 'performances.txt'), 'w') as file_perf:
    file_perf.write("Area under the ROC curve: " + str(AUC_ROC)
                    + "\nArea under Precision-Recall curve: " + str(AUC_prec_rec)
                    + "\nJaccard similarity score: " + str(jaccard_index)
                    + "\nF1 score (F-measure): " + str(F1_score)
                    + "\n\nConfusion matrix:"
                    + str(confusion)
                    + "\nACCURACY: " + str(accuracy)
                    + "\nSENSITIVITY: " + str(sensitivity)
                    + "\nSPECIFICITY: " + str(specificity)
                    + "\nPRECISION: " + str(precision)
                    )
