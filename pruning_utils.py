import csv
import json
import os
import random
import time
from datetime import datetime
from pathlib import Path

# matplotlib.use('agg')
import matplotlib.pyplot as plt
import numpy as np
import torch
import tqdm


def get_jaccard(y_true, y_pred, threshold=0.5):
    # y_pred = F.sigmoid(y_pred)
    y_pred = (y_pred > threshold).float()
    epsilon = 1e-6
    intersection = (y_pred * y_true).sum(dim=-2).sum(dim=-1)
    union = (y_pred + y_true).sum(dim=-2).sum(dim=-1)
    return list((intersection / (union - intersection + epsilon)).data.cpu().numpy())


def get_dice(y_true, y_pred, threshold=0.5):
    # y_pred = F.sigmoid(y_pred)
    y_pred = (y_pred > threshold).float()
    epsilon = 1e-6
    intersection = (y_pred * y_true).sum(dim=-2).sum(dim=-1)
    union = (y_pred + y_true).sum(dim=-2).sum(dim=-1)
    return list(((2. * intersection) / (union + epsilon)).data.cpu().numpy())


def get_sen(y_true, y_pred, threshold=0.5):
    # y_pred = F.sigmoid(y_pred)
    y_pred = (y_pred > threshold).float()
    epsilon = 1e-6
    TP = (((y_pred == 1) + (y_true == 1)) == 2).float().sum(dim=-2).sum(dim=-1)
    FN = (((y_pred == 0) + (y_true == 1)) == 2).float().sum(dim=-2).sum(dim=-1)
    return list((TP / (TP + FN + epsilon)).data.cpu().numpy())


def get_spe(y_true, y_pred, threshold=0.5):
    # y_pred = F.sigmoid(y_pred)
    y_pred = (y_pred > threshold).float()
    epsilon = 1e-6
    TN = (((y_pred == 0) + (y_true == 0)) == 2).float().sum(dim=-2).sum(dim=-1)
    FP = (((y_pred == 1) + (y_true == 0)) == 2).float().sum(dim=-2).sum(dim=-1)
    return list((TN / (TN + FP + epsilon)).data.cpu().numpy())


def get_ppv(y_true, y_pred, threshold=0.5):
    # y_pred = F.sigmoid(y_pred)
    y_pred = (y_pred > threshold).float()
    epsilon = 1e-6
    TP = (((y_pred == 1) + (y_true == 1)) == 2).float().sum(dim=-2).sum(dim=-1)
    FP = (((y_pred == 1) + (y_true == 0)) == 2).float().sum(dim=-2).sum(dim=-1)
    return list((TP / (TP + FP + epsilon)).data.cpu().numpy())


def get_acc(y_true, y_pred, threshold=0.5):
    y_pred = (y_pred > threshold).float()
    epsilon = 1e-6
    TP = (((y_pred == 1) + (y_true == 1)) == 2).float().sum(dim=-2).sum(dim=-1)
    FN = (((y_pred == 0) + (y_true == 1)) == 2).float().sum(dim=-2).sum(dim=-1)
    TN = (((y_pred == 0) + (y_true == 0)) == 2).float().sum(dim=-2).sum(dim=-1)
    FP = (((y_pred == 1) + (y_true == 0)) == 2).float().sum(dim=-2).sum(dim=-1)
    return list((TP + TN / (TP + FP + FN + TN + epsilon)).data.cpu().numpy())


def cuda(x):
    return x.cuda() if torch.cuda.is_available() else x


def write_event(log, step, **data):
    data['step'] = step
    data['dt'] = datetime.now().isoformat()
    log.write(json.dumps(data, sort_keys=True))
    log.write('\n')
    log.flush()


def check_crop_size(image_height, image_width):
    """Checks if image size divisible by 32.

    Args:
        image_height:
        image_width:

    Returns:
        True if both height and width divisible by 32 and False otherwise.

    """
    return image_height % 32 == 0 and image_width % 32 == 0


def train(args, model, criterion, train_loader, valid_loader, validation, init_optimizer, n_epochs=None, fold=None,
          num_classes=None):
    lr = args.lr
    n_epochs = n_epochs or args.n_epochs
    optimizer = init_optimizer(lr)

    train_log_path = './train_log/'
    valid_log_path = './valid_log/'

    root = Path(args.root)
    model_path = root / 'model_{fold}.pt'.format(fold=fold)
    if model_path.exists():
        state = torch.load(str(model_path))
        epoch = state['epoch']
        step = state['step']
        model.load_state_dict(state['model'])
        print('Restored model, epoch {}, step {:,}'.format(epoch, step))
    else:
        epoch = 1
        step = 0

    save = lambda ep: torch.save({
        'model': model.state_dict(),
        'epoch': ep,
        'step': step,
    }, str(model_path))

    report_each = 10
    log = root.joinpath('train_{fold}.log'.format(fold=fold)).open('at', encoding='utf8')
    valid_losses = []
    "-----------------------------train------------------------------------------"
    for epoch in range(epoch, n_epochs + 1):
        model.train()
        random.seed()
        tq = tqdm.tqdm(total=(len(train_loader) * args.batch_size))
        tq.set_description('Epoch {}, lr {}'.format(epoch, lr))
        "-------------------------性能指标---------------------------------"
        losses = []
        jaccard = []
        dice = []
        sen = []
        spe = []
        ppv = []
        acc = []
        tl = train_loader
        try:
            mean_loss = 0
            for i, (inputs, targets) in enumerate(tl):
                inputs = cuda(inputs)

                with torch.no_grad():
                    targets = cuda(targets)

                outputs = model(inputs)
                loss = criterion(outputs, targets)
                optimizer.zero_grad()
                batch_size = inputs.size(0)
                loss.backward()
                optimizer.step()
                step += 1
                tq.update(batch_size)
                losses.append(loss.item())

                mean_loss = np.mean(losses[-report_each:])
                jaccard += get_jaccard(targets, outputs.float(), 0.5)
                dice += get_dice(targets, outputs.float(), 0.5)
                sen += get_sen(targets, outputs.float(), 0.5)
                spe += get_spe(targets, outputs.float(), 0.5)
                ppv += get_ppv(targets, outputs.float(), 0.5)
                acc += get_acc(targets, outputs.float(), 0.5)

                tq.set_postfix(loss='{:.5f}'.format(mean_loss))
                if i and i % report_each == 0:
                    write_event(log, step, loss=mean_loss)

            "-----------------------------saveing train info--------------------------------"
            train_jaccard = np.mean(jaccard).astype(np.float64)
            train_dice = np.mean(dice).astype(np.float64)
            train_se = np.mean(sen).astype(np.float64)
            train_sp = np.mean(spe).astype(np.float64)
            train_pp = np.mean(ppv).astype(np.float64)
            print('train loss: {:.5f} | Jaccard: {:.5f} | DSC: {:.5f} | SEN: {:.5f} | SPE： {:.5f} | PPV: {:.5f}'.format(
                mean_loss, train_jaccard, train_dice, train_se, train_sp, train_pp))

            if epoch % 20 == 0:
                f = open(os.path.join(train_log_path, 'result.csv'), 'a', encoding='utf-8', newline='')
                wr = csv.writer(f)
                wr.writerow(["LikeNet", "epoch", epoch, "loss", mean_loss, "train_jaccard", train_jaccard, "train_dice",
                             train_dice, "train_se", train_se, "train_sp", train_sp, "train_pp", train_pp])
                f.close()

            write_event(log, step, loss=mean_loss)
            tq.close()

            "------------------------------Validation--------------------------------"
            valid_metrics = validation(model, criterion, valid_loader, num_classes)
            write_event(log, step, **valid_metrics)
            valid_loss = valid_metrics['valid_loss']
            valid_losses.append(valid_loss)

            valid_jaccard = valid_metrics['jaccard_loss']
            valid_dice = valid_metrics['dice']
            valid_se = valid_metrics['sen']
            valid_sp = valid_metrics['spe']
            valid_pp = valid_metrics['ppv']
            "-----------------------------saveing train info--------------------------------"
            if epoch % 20 == 0:
                f = open(os.path.join(valid_log_path, 'result.csv'), 'a', encoding='utf-8', newline='')
                wr = csv.writer(f)
                wr.writerow(
                    ["LikeNet", "epoch", epoch, "loss", valid_loss, "valid_jaccard", valid_jaccard, "valid_dice",
                     valid_dice, "valid_se", valid_se, "valid_sp", valid_sp, "valid_pp", valid_pp])
                f.close

            save(epoch + 1)
        except KeyboardInterrupt:
            tq.close()
            print('Ctrl+C, saving snapshot')
            save(epoch)
            print('done.')
            return


class AverageMeter(object):
    """Computes and stores the average and current value"""

    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count


class RecorderMeter(object):
    """Computes and stores the minimum loss value and its epoch index"""

    def __init__(self, total_epoch):
        self.reset(total_epoch)

    def reset(self, total_epoch):
        assert total_epoch > 0
        self.total_epoch = total_epoch
        self.current_epoch = 0
        self.epoch_losses = np.zeros((self.total_epoch, 2), dtype=np.float32)  # [epoch, train/val]
        self.epoch_losses = self.epoch_losses - 1

        self.epoch_accuracy = np.zeros((self.total_epoch, 2), dtype=np.float32)  # [epoch, train/val]
        self.epoch_accuracy = self.epoch_accuracy

    def update(self, idx, train_loss, train_acc, val_loss, val_acc):
        assert idx >= 0 and idx < self.total_epoch, 'total_epoch : {} , but update with the {} index'.format(
            self.total_epoch, idx)
        self.epoch_losses[idx, 0] = train_loss
        self.epoch_losses[idx, 1] = val_loss
        self.epoch_accuracy[idx, 0] = train_acc
        self.epoch_accuracy[idx, 1] = val_acc
        self.current_epoch = idx + 1
        return self.max_accuracy(False) == val_acc

    def max_accuracy(self, istrain):
        if self.current_epoch <= 0: return 0
        if istrain:
            return self.epoch_accuracy[:self.current_epoch, 0].max()
        else:
            return self.epoch_accuracy[:self.current_epoch, 1].max()

    def plot_curve(self, save_path):
        title = 'the accuracy/loss curve of train/val'
        dpi = 80
        width, height = 1200, 800
        legend_fontsize = 10
        scale_distance = 48.8
        figsize = width / float(dpi), height / float(dpi)

        fig = plt.figure(figsize=figsize)
        x_axis = np.array([i for i in range(self.total_epoch)])  # epochs
        y_axis = np.zeros(self.total_epoch)

        plt.xlim(0, self.total_epoch)
        plt.ylim(0, 100)
        interval_y = 5
        interval_x = 5
        plt.xticks(np.arange(0, self.total_epoch + interval_x, interval_x))
        plt.yticks(np.arange(0, 100 + interval_y, interval_y))
        plt.grid()
        plt.title(title, fontsize=20)
        plt.xlabel('the training epoch', fontsize=16)
        plt.ylabel('accuracy', fontsize=16)

        y_axis[:] = self.epoch_accuracy[:, 0]
        plt.plot(x_axis, y_axis, color='g', linestyle='-', label='train-accuracy', lw=2)
        plt.legend(loc=4, fontsize=legend_fontsize)

        y_axis[:] = self.epoch_accuracy[:, 1]
        plt.plot(x_axis, y_axis, color='y', linestyle='-', label='valid-accuracy', lw=2)
        plt.legend(loc=4, fontsize=legend_fontsize)

        y_axis[:] = self.epoch_losses[:, 0]
        plt.plot(x_axis, y_axis * 50, color='g', linestyle=':', label='train-loss-x50', lw=2)
        plt.legend(loc=4, fontsize=legend_fontsize)

        y_axis[:] = self.epoch_losses[:, 1]
        plt.plot(x_axis, y_axis * 50, color='y', linestyle=':', label='valid-loss-x50', lw=2)
        plt.legend(loc=4, fontsize=legend_fontsize)

        if save_path is not None:
            fig.savefig(save_path, dpi=dpi, bbox_inches='tight')
            print('---- save figure {} into {}'.format(title, save_path))
        plt.close(fig)


def time_string():
    ISOTIMEFORMAT = '%Y-%m-%d %X'
    string = '[{}]'.format(time.strftime(ISOTIMEFORMAT, time.gmtime(time.time())))
    return string


def convert_secs2time(epoch_time):
    need_hour = int(epoch_time / 3600)
    need_mins = int((epoch_time - 3600 * need_hour) / 60)
    need_secs = int(epoch_time - 3600 * need_hour - 60 * need_mins)
    return need_hour, need_mins, need_secs


def time_file_str():
    ISOTIMEFORMAT = '%Y-%m-%d'
    string = '{}'.format(time.strftime(ISOTIMEFORMAT, time.gmtime(time.time())))
    return string + '-{}'.format(random.randint(1, 10000))


def timing(f):
    def wrap(*args):
        time1 = time.time()
        ret = f(*args)
        time2 = time.time()
        print('%s function took %0.3f ms' % (f.__name__, (time2 - time1) * 1000.0))
        return ret

    return wrap
