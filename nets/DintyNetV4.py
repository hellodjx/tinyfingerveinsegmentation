import torch
import torch.nn as nn
import torch.nn.functional as F
import math


def _make_divisible(v, divisor, min_value=None):
    """
    This function is taken from the original tf repo.
    It ensures that all layers have a channel number that is divisible by 8
    It can be seen here:
    https://github.com/tensorflow/models/blob/master/research/slim/nets/mobilenet/mobilenet.py
    """
    if min_value is None:
        min_value = divisor
    new_v = max(min_value, int(v + divisor / 2) // divisor * divisor)
    # Make sure that round down does not go down by more than 10%.
    if new_v < 0.9 * v:
        new_v += divisor
    return new_v


def channel_shuffle(x, groups):
    batchsize, num_channels, height, width = x.data.size()

    channels_per_group = num_channels // groups

    # reshape
    x = x.view(batchsize, groups,
               channels_per_group, height, width)

    # transpose
    # - contiguous() required if transpose() is used before view().
    #   See https://github.com/pytorch/pytorch/issues/764
    x = torch.transpose(x, 1, 2).contiguous()

    # flatten
    x = x.view(batchsize, -1, height, width)

    return x


# 激活函数
class HSwish(nn.Module):
    def forward(self, x):
        out = x * F.relu6(x + 3, inplace=True) / 6
        return out


class HSigmoid(nn.Module):
    def forward(self, x):
        out = F.relu6(x + 3, inplace=True) / 6
        return out


# se注意力model
class SeModule(nn.Module):
    def __init__(self, in_size, reduction=4):
        super(SeModule, self).__init__()
        self.se = nn.Sequential(
            # 平均池化
            nn.AdaptiveAvgPool2d(1),
            nn.Conv2d(in_size, in_size // reduction, kernel_size=1, stride=1, padding=0, bias=False),
            nn.BatchNorm2d(in_size // reduction),
            HSigmoid(),
            nn.Conv2d(in_size // reduction, in_size, kernel_size=1, stride=1, padding=0, bias=False),
            nn.BatchNorm2d(in_size),
            HSigmoid()
        )

    def forward(self, x):
        return x * self.se(x)


class SandGlassBlock(nn.Module):
    def __init__(self, inp_c, outp_c, stride, expand_ratio, keep_3x3=False):
        super(SandGlassBlock, self).__init__()
        assert stride in [1, 2]

        hidden_dim = inp_c // expand_ratio
        if hidden_dim < outp_c / 6.:
            hidden_dim = math.ceil(outp_c / 6.)
            hidden_dim = _make_divisible(hidden_dim, 16)  # + 16
        hidden_dim = int(hidden_dim)

        self.expand_ratio = expand_ratio
        if expand_ratio == 2:
            self.conv = nn.Sequential(
                # dw
                nn.Conv2d(inp_c, inp_c, 3, 1, 1, groups=inp_c, bias=False),
                nn.BatchNorm2d(inp_c),
                nn.ReLU6(inplace=True),
                # pw-linear
                nn.Conv2d(inp_c, hidden_dim, 1, 1, 0, bias=False),
                nn.BatchNorm2d(hidden_dim),
                # pw-linear
                nn.Conv2d(hidden_dim, outp_c, 1, 1, 0, bias=False),
                nn.BatchNorm2d(outp_c),
                nn.ReLU6(inplace=True),
                # dw
                nn.Conv2d(outp_c, outp_c, 3, stride, 1, groups=outp_c, bias=False),
                nn.BatchNorm2d(outp_c),
            )
        elif inp_c != outp_c and stride == 1 and not keep_3x3:
            self.conv = nn.Sequential(
                # pw-linear
                nn.Conv2d(inp_c, hidden_dim, 1, 1, 0, bias=False),
                nn.BatchNorm2d(hidden_dim),
                # pw-linear
                nn.Conv2d(hidden_dim, outp_c, 1, 1, 0, bias=False),
                nn.BatchNorm2d(outp_c),
                nn.ReLU6(inplace=True),
            )
        elif inp_c != outp_c and stride == 2 and not keep_3x3:
            self.conv = nn.Sequential(
                # pw-linear
                nn.Conv2d(inp_c, hidden_dim, 1, 1, 0, bias=False),
                nn.BatchNorm2d(hidden_dim),
                # pw-linear
                nn.Conv2d(hidden_dim, outp_c, 1, 1, 0, bias=False),
                nn.BatchNorm2d(outp_c),
                nn.ReLU6(inplace=True),
                # dw
                nn.Conv2d(outp_c, outp_c, 3, stride, 1, groups=outp_c, bias=False),
                nn.BatchNorm2d(outp_c),
            )
        else:
            self.conv = nn.Sequential(
                # dw
                nn.Conv2d(inp_c, inp_c, 3, 1, 1, groups=inp_c, bias=False),
                nn.BatchNorm2d(inp_c),
                nn.ReLU6(inplace=True),
                # pw
                nn.Conv2d(inp_c, hidden_dim, 1, 1, 0, bias=False),
                nn.BatchNorm2d(hidden_dim),
                # nn.ReLU6(inplace=True),
                # pw
                nn.Conv2d(hidden_dim, outp_c, 1, 1, 0, bias=False),
                nn.BatchNorm2d(outp_c),
                nn.ReLU6(inplace=True),
                # dw
                nn.Conv2d(outp_c, outp_c, 3, 1, 1, groups=outp_c, bias=False),
                nn.BatchNorm2d(outp_c),
            )

    def forward(self, x):
        out = self.conv(x)
        return out


class DecoderBlock(nn.Module):
    """
    Decoder block: upsample and concatenate with features maps from the encoder part
    """

    def __init__(self, up_in_c, x_in_c, kernel_size, stride, expand_ratio, up_sample_mode='bilinear', se_module=None,
                 keep_3x3=False):
        super().__init__()
        self.up_sample = nn.Upsample(scale_factor=2, mode=up_sample_mode, align_corners=False)
        self.sg = SandGlassBlock(up_in_c, x_in_c, stride, expand_ratio, keep_3x3)
        self.se = se_module

    def forward(self, up_in, x_in):
        if up_in.shape == x_in.shape:
            up_out = up_in
        else:
            up_out = self.up_sample(up_in)

        cat_x = torch.cat([up_out, x_in], dim=1)
        x = self.sg(cat_x)
        if self.se is not None:
            x = self.se(x)
        # TODO below code need experiment
        # x2 = x + cat_x
        # return x2
        return x


class LastDecoderBlock(nn.Module):
    def __init__(self, x_in_c, upsample_mode='bilinear', out_c=1):
        super().__init__()
        self.upsample = nn.Upsample(scale_factor=2, mode=upsample_mode, align_corners=False)  # H, W -> 2H, 2W

        self.Conv_1x1 = nn.Conv2d(x_in_c, out_c, kernel_size=1, stride=1, padding=0)

    def forward(self, up_in, x_in):
        up_out = self.upsample(up_in)
        cat_x = torch.cat([up_out, x_in], dim=1)
        x = self.Conv_1x1(cat_x)
        return x


class EncoderBlock(nn.Module):
    def __init__(self, inp_c, outp_c, kernel_size, stride, expand_ratio, se_module=None, keep_3x3=False):
        super().__init__()
        self.sg = SandGlassBlock(inp_c, outp_c, stride, expand_ratio, keep_3x3)
        self.se = se_module

    def forward(self, x_in):
        x = self.sg(x_in)
        if self.se is not None:
            x = self.se(x)
        return x


class DintyNetV4(nn.Module):
    def __init__(self, upsample_mode='bilinear'):
        super(DintyNetV4, self).__init__()
        # inp_c, outp_c, kernel_size, stride, expand_ratio, se_module=None, keep_3x3=False
        self.conv0 = EncoderBlock(1, 64, 3, 2, 2, None, False)
        self.conv1 = EncoderBlock(64, 128, 3, 1, 6, SeModule(128), True)
        self.conv2 = EncoderBlock(128, 256, 3, 2, 6, SeModule(256), True)
        self.conv3 = EncoderBlock(256, 512, 3, 1, 6, SeModule(512), True)
        self.conv4 = EncoderBlock(512, 512, 3, 2, 6, SeModule(512), True)

        # up_in_c, x_in_c, kernel_size, stride, expand_ratio, up_sample_mode, se_module=None, keep_3x3=False
        self.decode4 = DecoderBlock(1024, 256, 3, 2, 6, upsample_mode, SeModule(256), True)
        self.decode3 = DecoderBlock(512, 128, 3, 1, 6, upsample_mode, SeModule(128), True)
        self.decode2 = DecoderBlock(256, 64, 3, 2, 6, upsample_mode, SeModule(64), True)
        self.decode1 = DecoderBlock(128, 32, 3, 1, 6, upsample_mode, None, True)
        self.decode_last = LastDecoderBlock(33, upsample_mode, out_c=1)

    def forward(self, x):
        conv0 = self.conv0(x)
        conv1 = self.conv1(conv0)
        conv2 = self.conv2(conv1)
        conv3 = self.conv3(conv2)
        conv4 = self.conv4(conv3)

        decode4 = self.decode4(conv4, conv3)
        # print(conv0.shape, conv1.shape, conv2.shape, conv3.shape, conv4.shape, decode4.shape)
        decode3 = self.decode3(decode4, conv2)
        decode2 = self.decode2(decode3, conv1)
        decode1 = self.decode1(decode2, conv0)
        decode0 = self.decode_last(decode1, x)

        decode = torch.sigmoid(decode0)

        return decode
