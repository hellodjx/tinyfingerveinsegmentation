# @Name: M3U_Inception
# @Description: 
# @Author: JorVer
# @Date: 2019/9/21

import torch
import torch.nn as nn
import torch.nn.functional as F
import math


def _make_divisible(v, divisor, min_value=None):
    """
    This function is taken from the original tf repo.
    It ensures that all layers have a channel number that is divisible by 8
    It can be seen here:
    https://github.com/tensorflow/models/blob/master/research/slim/nets/mobilenet/mobilenet.py
    """
    if min_value is None:
        min_value = divisor
    new_v = max(min_value, int(v + divisor / 2) // divisor * divisor)
    # Make sure that round down does not go down by more than 10%.
    if new_v < 0.9 * v:
        new_v += divisor
    return new_v


def channel_shuffle(x, groups):
    batchsize, num_channels, height, width = x.data.size()

    channels_per_group = num_channels // groups

    # reshape
    x = x.view(batchsize, groups,
               channels_per_group, height, width)

    # transpose
    # - contiguous() required if transpose() is used before view().
    #   See https://github.com/pytorch/pytorch/issues/764
    x = torch.transpose(x, 1, 2).contiguous()

    # flatten
    x = x.view(batchsize, -1, height, width)

    return x


# 激活函数
class HSwish(nn.Module):
    def forward(self, x):
        out = x * F.relu6(x + 3, inplace=True) / 6
        return out


class HSigmoid(nn.Module):
    def forward(self, x):
        out = F.relu6(x + 3, inplace=True) / 6
        return out


# se注意力model
class SeModule(nn.Module):
    def __init__(self, in_size, reduction=4):
        super(SeModule, self).__init__()
        self.se = nn.Sequential(
            # 平均池化
            nn.AdaptiveAvgPool2d(1),
            nn.Conv2d(in_size, in_size // reduction, kernel_size=1, stride=1, padding=0, bias=False),
            nn.BatchNorm2d(in_size // reduction),
            HSigmoid(),
            nn.Conv2d(in_size // reduction, in_size, kernel_size=1, stride=1, padding=0, bias=False),
            nn.BatchNorm2d(in_size),
            HSigmoid()
        )

    def forward(self, x):
        return x * self.se(x)


class InvertedResidualup(nn.Module):
    def __init__(self, kernel_size, in_size, out_size, ratio, dw_size, expand_ratio, stride):
        super(InvertedResidualup, self).__init__()
        self.stride = stride
        self.kernel_size = kernel_size
        self.out_size = out_size
        init_channels = math.ceil(out_size / ratio)
        new_channels = init_channels * (ratio - 1)
        hidden_dim = round(init_channels * expand_ratio)

        # ex
        self.conv1 = nn.Conv2d(in_size, hidden_dim, kernel_size=1, stride=1, padding=0, groups=4, bias=False)
        self.bn1 = nn.BatchNorm2d(hidden_dim)

        self.conv2 = nn.Conv2d(hidden_dim, hidden_dim, kernel_size=kernel_size, stride=stride,
                               padding=kernel_size // 2, groups=4, bias=False)
        self.bn2 = nn.BatchNorm2d(hidden_dim)

        # pw
        self.conv3 = nn.Conv2d(hidden_dim, init_channels, kernel_size=1, stride=1, padding=0, bias=False)
        self.bn3 = nn.BatchNorm2d(init_channels)

        self.cheap_operation = nn.Sequential(
            nn.Conv2d(init_channels, new_channels, dw_size, 1, dw_size // 2, groups=4, bias=False),
            nn.BatchNorm2d(new_channels),
            nn.ReLU(inplace=True)
        )

    def forward(self, x):
        out = self.bn1(self.conv1(x))
        out = channel_shuffle(out, 4)
        out = self.bn2(self.conv2(out))
        out1 = self.bn3(self.conv3(out))
        out2 = self.cheap_operation(out1)
        out = torch.cat([out1, out2], dim=1)
        return out[:, :self.out_size, :, :]


class InvertedResidualdown(nn.Module):
    def __init__(self, kernel_size, in_size, out_size, ratio, dw_size, expand_ratio, stride):
        super(InvertedResidualdown, self).__init__()
        self.stride = stride
        self.kernel_size = kernel_size
        self.out_size = out_size
        init_channels = math.ceil(out_size / ratio)
        new_channels = init_channels * (ratio - 1)
        # self.time = time
        hidden_dim = round(init_channels * expand_ratio)

        # ex
        if in_size == 1:
            self.conv1 = nn.Sequential(
                nn.Conv2d(in_size, hidden_dim, 3, 1, 1, bias=False),
                nn.BatchNorm2d(hidden_dim),
                nn.ReLU(inplace=True),
            )
        else:
            self.conv1 = nn.Conv2d(in_size, hidden_dim, kernel_size=1, stride=1, padding=0, groups=4, bias=False)

        self.bn1 = nn.BatchNorm2d(hidden_dim)

        self.conv2 = nn.Conv2d(hidden_dim, hidden_dim, kernel_size=kernel_size, stride=stride,
                               padding=kernel_size // 2, groups=4, bias=False)
        self.bn2 = nn.BatchNorm2d(hidden_dim)

        # pw
        self.conv3 = nn.Conv2d(hidden_dim, init_channels, kernel_size=1, stride=1, padding=0, groups=4, bias=False)
        self.bn3 = nn.BatchNorm2d(init_channels)

        self.cheap_operation = nn.Sequential(
            nn.Conv2d(init_channels, new_channels, dw_size, 1, dw_size // 2, groups=4, bias=False),
            nn.BatchNorm2d(new_channels),
            nn.ReLU(inplace=True)
        )

    def forward(self, x):
        out = self.bn1(self.conv1(x))
        out = channel_shuffle(out, 4)
        out = self.bn2(self.conv2(out))
        out1 = self.bn3(self.conv3(out))
        out2 = self.cheap_operation(out1)
        out = torch.cat([out1, out2], dim=1)

        return out[:, :self.out_size, :, :]


class DecoderBlock(nn.Module):
    """
    Decoder block: upsample and concatenate with features maps from the encoder part
    """

    def __init__(self, kernel_size, up_in_c, x_in_c, upsample_mode='bilinear', expand_ratio=0.2, se_module=None):
        super().__init__()
        self.upsample = nn.Upsample(scale_factor=2, mode=upsample_mode, align_corners=False)  # H, W -> 2H, 2W
        self.ir1 = InvertedResidualup(kernel_size, up_in_c, x_in_c, ratio=2, dw_size=3, expand_ratio=expand_ratio,
                                      stride=1, )
        self.se = se_module
        self.shortcut = nn.Sequential(
            nn.Conv2d(up_in_c, up_in_c, 3, stride=1,
                      padding=(3 - 1) // 2, groups=int(up_in_c / 2), bias=False),
            nn.BatchNorm2d(up_in_c),
            nn.Conv2d(up_in_c, x_in_c, 1, stride=1, padding=0, groups=4, bias=False),
            nn.BatchNorm2d(x_in_c),
        )

    def forward(self, up_in, x_in):
        up_out = self.upsample(up_in)
        cat_x = torch.cat([up_out, x_in], dim=1)
        x = self.ir1(cat_x)
        if self.se is not None:
            x = self.se(x)
        x1 = self.shortcut(cat_x)
        x2 = x + x1
        return x2


class LastDecoderBlock(nn.Module):
    def __init__(self, x_in_c, upsample_mode='bilinear', expand_ratio=0.2, out_c=1):
        super().__init__()
        self.upsample = nn.Upsample(scale_factor=2, mode=upsample_mode, align_corners=False)  # H, W -> 2H, 2W

        self.Conv_1x1 = nn.Conv2d(x_in_c, 1, kernel_size=1, stride=1, padding=0)

    def forward(self, up_in, x_in):
        up_out = self.upsample(up_in)
        cat_x = torch.cat([up_out, x_in], dim=1)
        x = self.Conv_1x1(cat_x)
        return x


class InvertedResidualup_Last(nn.Module):
    def __init__(self, inp, oup, stride, expand_ratio):
        super(InvertedResidualup_Last, self).__init__()
        self.stride = stride

        hidden_dim = round(inp * expand_ratio)

        # Bottleneck with expansion layer
        self.conv = nn.Sequential(
            # pw
            nn.Conv2d(inp, hidden_dim, 1, 1, 0, bias=False),
            nn.BatchNorm2d(hidden_dim),
            HSwish(),
            # dw
            nn.Conv2d(hidden_dim, hidden_dim, 3, stride, 1, groups=hidden_dim, bias=False),
            nn.BatchNorm2d(hidden_dim),
            HSwish(),
            # pw-linear
            nn.Conv2d(hidden_dim, oup, 1, 1, 0, bias=False),
            nn.BatchNorm2d(oup),
        )

    def forward(self, x):
        return self.conv(x)


class EncoderBlock(nn.Module):
    def __init__(self, kernel_size, inp_c, outp_c, ratio, dw_size, stride, expand_ratio, nolinear=None, se_module=None,
                 cat=False):
        super().__init__()
        # kernel_size, in_size, out_size, expand_ratio, nolinear, stride
        self.ir1 = InvertedResidualdown(kernel_size, inp_c, outp_c, ratio=ratio, dw_size=3, expand_ratio=expand_ratio,
                                        stride=2)
        self.se = se_module
        self.cat = cat
        if inp_c == 1:
            self.shortcut = nn.Sequential(
                nn.Conv2d(inp_c, inp_c, dw_size, stride=stride,
                          padding=(dw_size - 1) // 2, groups=inp_c, bias=False),
                nn.BatchNorm2d(inp_c),
                nn.Conv2d(inp_c, outp_c, 1, stride=1, padding=0, bias=False),
                nn.BatchNorm2d(outp_c),
            )
        else:
            self.shortcut = nn.Sequential(
                nn.Conv2d(inp_c, int(inp_c / 4), kernel_size=1, groups=4, bias=False),
                nn.BatchNorm2d(int(inp_c / 4)),
                nn.ReLU(inplace=True),
                nn.Conv2d(int(inp_c / 4), int(inp_c / 4), dw_size, stride=stride,
                          padding=(dw_size - 1) // 2, groups=4, bias=False),
                nn.BatchNorm2d(int(inp_c / 4)),
                nn.Conv2d(int(inp_c / 4), outp_c, 1, stride=1, padding=0, groups=4, bias=False),
                nn.BatchNorm2d(outp_c),
                nn.ReLU(inplace=True),

            )

    def forward(self, x_in):
        residual = x_in
        x = self.ir1(x_in)
        if self.se is not None:
            x = self.se(x)
        x1 = self.shortcut(residual)
        x = x + x1
        return x


class DintyNetV3(nn.Module):
    def __init__(self, upsample_mode='bilinear', expand_ratio=0.5):
        super(DintyNetV3, self).__init__()
        # kernel_size, inp_c, outp_c, time=6, expand_ratio, nolinear=None, semodule=None, cat=False
        self.conv0 = EncoderBlock(3, 1, 64, 2, 3, 2, 1, HSwish(), None, False)  # 272
        self.conv1 = EncoderBlock(3, 64, 128, 2, 3, 2, 0.5, HSwish(), SeModule(128), False)  # 136
        self.conv2 = EncoderBlock(3, 128, 256, 2, 3, 2, 0.5, HSwish(), SeModule(256), False)  # 68
        self.conv3 = EncoderBlock(3, 256, 512, 2, 3, 2, 0.5, HSwish(), SeModule(512), False)  # 34
        self.conv4 = EncoderBlock(3, 512, 512, 2, 3, 2, 0.5, HSwish(), SeModule(512), False)  # 17

        self.decode4 = DecoderBlock(3, 1024, 256, upsample_mode, expand_ratio, SeModule(256))
        self.decode3 = DecoderBlock(3, 512, 128, upsample_mode, expand_ratio, SeModule(128))
        self.decode2 = DecoderBlock(3, 256, 64, upsample_mode, expand_ratio, SeModule(64))
        self.decode1 = DecoderBlock(3, 128, 32, upsample_mode, expand_ratio, None)
        self.decode_last = LastDecoderBlock(33, upsample_mode, expand_ratio, out_c=1)

    def forward(self, x):
        conv0 = self.conv0(x)
        conv1 = self.conv1(conv0)
        conv2 = self.conv2(conv1)
        conv3 = self.conv3(conv2)
        conv4 = self.conv4(conv3)

        decode4 = self.decode4(conv4, conv3)
        decode3 = self.decode3(decode4, conv2)
        decode2 = self.decode2(decode3, conv1)
        decode1 = self.decode1(decode2, conv0)
        decode0 = self.decode_last(decode1, x)

        decode = torch.sigmoid(decode0)

        return decode
